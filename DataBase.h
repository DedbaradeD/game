#pragma once

#include <QtSql/QSqlDatabase>

class DataBase
{
public:
    explicit DataBase();
    ~DataBase();

    QStringList getItemData(int itemId);
    QStringList getPreviousData(int previousRow, int previousColumn);
    QStringList getInventoryData(int row, int column);
    void removeItem(int itemsCount, int row, int column);
    void updateInventoryData(int itemsCount, int row, int column, int previousRow, int previousColumn);
    void clearInventoryData();

private:
    QSqlDatabase _database;
};
