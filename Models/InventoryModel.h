﻿#pragma once

#include <QSharedPointer>
#include <QStandardItemModel>

class InventoryObject;
typedef QSharedPointer<InventoryObject> InventoryObjectShp;

class InventoryModel : public QStandardItemModel
{
    Q_OBJECT

public:
    InventoryModel(QObject *parent = nullptr);
    ~InventoryModel();

    void setDefaultData();
    void updateInventoryData(InventoryObjectShp obj, QString imagePath);
};


