﻿#include "ItemModel.h"

#include "Objects/ItemObject.h"


ItemModel::ItemModel(QObject* parent)
    : QStandardItemModel(parent)
{
}

ItemModel::~ItemModel()
{
}

// задаем данные модели айтема (яблока)
void ItemModel::setItemObject(ItemObjectShp obj)
{
    QStandardItem *item = new QStandardItem();
    item->setIcon(QIcon(QPixmap(obj->_imagePath)));
    item->setText("1");
    item->setTextAlignment(Qt::AlignBottom | Qt::AlignRight);

    this->appendRow(item);
}
