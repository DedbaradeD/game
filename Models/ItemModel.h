﻿#pragma once

#include <QSharedPointer>
#include <QStandardItemModel>

class ItemObject;
typedef QSharedPointer<ItemObject> ItemObjectShp;

class ItemModel : public QStandardItemModel
{
    Q_OBJECT

public:
    ItemModel(QObject *parent = nullptr);
    ~ItemModel();

    void setItemObject(ItemObjectShp obj);
};


