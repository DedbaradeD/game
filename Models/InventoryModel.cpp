﻿#include "InventoryModel.h"

#include "Objects/InventoryObject.h"


InventoryModel::InventoryModel(QObject* parent)
    : QStandardItemModel(parent)
{
}

InventoryModel::~InventoryModel()
{
}

// устанавливаем данные инвентаря по умолчанию
void InventoryModel::setDefaultData()
{
    for (int i = 0; i < 3; i++)
    {
        QList<QStandardItem*> items;
        items.append(new QStandardItem());
        items.append(new QStandardItem());
        items.append(new QStandardItem());
        this->appendRow(items);
    }
}

// обновляем данные модели инвентаря
void InventoryModel::updateInventoryData(InventoryObjectShp obj, QString imagePath)
{
    if (obj->_itemsCount == 0)
    {
        setItem(obj->_row, obj->_column, new QStandardItem());
    }
    else
    {
        auto item = this->itemFromIndex(this->index(obj->_row, obj->_column));
        item->setIcon(QIcon(QPixmap(imagePath)));
        item->setText(QString::number(obj->_itemsCount));
        item->setTextAlignment(Qt::AlignBottom | Qt::AlignRight);
    }
}
