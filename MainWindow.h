#pragma once

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QStandardItemModel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setItemModel(QStandardItemModel *itemModel);
    void setInventoryModel(QStandardItemModel *inventoryModel);
    void changeCurrentIndex(QModelIndex index);
    bool eventFilter(QObject *object, QEvent *event) override;

private:
    Ui::MainWindow *_ui;

protected slots:
    void newGame();
    void exit();
    void showMenu();

signals:
    void setInventoryData(int row, int column);
    void rightButtonClick(int row, int column);
    void setPreviousData(int previousRow, int previousColumn);
    void dropItem(int row, int column, int previousRow, int previousColumn);
    void clearInventory();
};
