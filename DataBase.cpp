#include "DataBase.h"
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

DataBase::DataBase()
{
    _database = QSqlDatabase::addDatabase("QSQLITE");
    _database.setDatabaseName("GameDB");
    if(!_database.open())
    {
        qDebug() << _database.lastError().text();
    }
}

DataBase::~DataBase()
{

}

// получаем данные айтема (яблока)
QStringList DataBase::getItemData(int itemId)
{
    QStringList itemData;
    QSqlQuery query;
    query.prepare("SELECT type, image "
                  "FROM item "
                  "WHERE id = :id");
    query.bindValue(":id", itemId);
    if(query.exec())
    {
        while (query.next())
        {
            itemData.append(query.value(0).toString());
            itemData.append(query.value(1).toString());
        }
    }
    return itemData;
}

// получаем данные инвентаря
QStringList DataBase::getInventoryData(int row, int column)
{
    QStringList inventoryData;
    QSqlQuery query;
    query.prepare("SELECT size, row, column, count, type "
                  "FROM inventory "
                  "WHERE row = :row AND column = :column");
    query.bindValue(":row", row);
    query.bindValue(":column", column);
    if(query.exec())
    {
        while (query.next())
        {
            inventoryData.append(query.value(0).toString());
            inventoryData.append(query.value(1).toString());
            inventoryData.append(query.value(2).toString());
            inventoryData.append(query.value(3).toString());
            inventoryData.append(query.value(4).toString());
        }
    }
    return inventoryData;
}

// обновляем данные инвентаря при перемещении айтема(ов) (яблока\яблок)
void DataBase::updateInventoryData(int itemsCount, int row, int column, int previousRow, int previousColumn)
{
    QSqlQuery query;
    query.prepare("UPDATE inventory "
                  "SET count = :count "
                  "WHERE row = :row AND column = :column");
    query.bindValue(":row", row);
    query.bindValue(":column", column);
    query.bindValue(":count", itemsCount);
    query.exec();

    QSqlQuery deleteQuery;
    deleteQuery.prepare("UPDATE inventory "
                        "SET count = :count "
                        "WHERE row = :row AND column = :column");
    deleteQuery.bindValue(":row", previousRow);
    deleteQuery.bindValue(":column", previousColumn);
    deleteQuery.bindValue(":count", 0);
    deleteQuery.exec();
}

// получаем данные перемещаемого(ых) айтема(ов)
QStringList DataBase::getPreviousData(int previousRow, int previousColumn)
{
    QStringList inventoryData;
    QSqlQuery query;
    query.prepare("SELECT size, row, column, count, type "
                  "FROM inventory "
                  "WHERE row = :row AND column = :column");
    query.bindValue(":row", previousRow);
    query.bindValue(":column", previousColumn);
    if(query.exec())
    {
        while (query.next())
        {
            inventoryData.append(query.value(0).toString());
            inventoryData.append(query.value(1).toString());
            inventoryData.append(query.value(2).toString());
            inventoryData.append(query.value(3).toString());
            inventoryData.append(query.value(4).toString());
        }
    }
    return inventoryData;
}

// очищаем инвентарь
void DataBase::clearInventoryData()
{
    QSqlQuery query;
    query.prepare("UPDATE inventory "
                  "SET count = :count");
    query.bindValue(":count", 0);
    query.exec();
}

// удаляем айтем (яблоко)
void DataBase::removeItem(int itemsCount, int row, int column)
{
    QSqlQuery query;
    query.prepare("UPDATE inventory "
                  "SET count = :count "
                  "WHERE row = :row AND column = :column");
    query.bindValue(":row", row);
    query.bindValue(":column", column);
    query.bindValue(":count", itemsCount);
    query.exec();
}
