#pragma once

#include <QSharedPointer>

class ItemObject
{
public:
    explicit ItemObject();
    ~ItemObject();

public:
    QString _itemType;
    QString _imagePath;
};

typedef QSharedPointer<ItemObject> ItemObjectShp;
Q_DECLARE_METATYPE(ItemObjectShp);
