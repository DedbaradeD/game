#pragma once

#include <QSharedPointer>

class InventoryObject
{
public:
    explicit InventoryObject();
    ~InventoryObject();

public:
    quint64 _size;
    quint64 _row;
    quint64 _column;
    quint64 _itemsCount;
    QString _itemType;
};

typedef QSharedPointer<InventoryObject> InventoryObjectShp;
Q_DECLARE_METATYPE(InventoryObjectShp);
