#pragma once

#include <QObject>
#include <QStringList>

class MainWindow;
class DataBase;
class InventoryModel;
class ItemModel;

class Controller : public QObject
{
    Q_OBJECT

public:
    explicit Controller(MainWindow *mainWindow);
    ~Controller();

    void setItemObj(QStringList &itemData);

private:
    MainWindow *_mainWindow = nullptr;
    DataBase *_dataBase = nullptr;
    InventoryModel *_inventoryModel = nullptr;
    ItemModel *_itemModel = nullptr;
    QStringList _itemData;
    QStringList _inventoryData;
    QStringList _previousData;

protected slots:
    void setInventoryData(int row, int column);
    void deleteItem(int row, int column);
    void setPreviousData(int previousRow, int previousColumn);
    void addItem(int row, int column, int previousRow, int previousColumn);
    void clearInventory();
};
