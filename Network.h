#pragma once

class QTcpServer;
class QTcpSocket;

class Network
{
public:
    explicit Network();
    ~Network();

private:
    QTcpServer *_server = nullptr;
    QTcpSocket *_socket = nullptr;
};
