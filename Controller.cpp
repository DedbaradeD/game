#include "Controller.h"
#include "DataBase.h"
#include "Network.h"
#include "MainWindow.h"

#include "Models/InventoryModel.h"
#include "Models/ItemModel.h"

#include "Objects/InventoryObject.h"
#include "Objects/ItemObject.h"

#include <QSound>

Controller::Controller(MainWindow *mainWindow):
    _mainWindow(mainWindow)
{
    _dataBase = new DataBase();
    _inventoryModel = new InventoryModel(this);
    _itemModel = new ItemModel(this);

    _dataBase->clearInventoryData();

    _itemData = _dataBase->getItemData(1);
    setItemObj(_itemData);
    _inventoryModel->setDefaultData();

    _mainWindow->setItemModel(_itemModel);
    _mainWindow->setInventoryModel(_inventoryModel);

    connect(_mainWindow, &MainWindow::setInventoryData, this, &Controller::setInventoryData);
    connect(_mainWindow, &MainWindow::rightButtonClick, this, &Controller::deleteItem);
    connect(_mainWindow, &MainWindow::setPreviousData, this, &Controller::setPreviousData);
    connect(_mainWindow, &MainWindow::dropItem, this, &Controller::addItem);
    connect(_mainWindow, &MainWindow::clearInventory, this, &Controller::clearInventory);
}

Controller::~Controller()
{
    delete _dataBase;
}

// задаем данные объекта айтема (яблока)
void Controller::setItemObj(QStringList &itemData)
{
    ItemObjectShp obj = ItemObjectShp::create();
    obj->_itemType = itemData.value(0);
    obj->_imagePath = itemData.value(1);
    _itemModel->setItemObject(obj);
}

// задаем данные перемещаемого(ых) айтема(ов) (яблока\яблок)
void Controller::setInventoryData(int row, int column)
{
    _inventoryData = _dataBase->getInventoryData(row, column);
}

// удаляем айтем из модели инвентаря
void Controller::deleteItem(int row, int column)
{
    if (_inventoryData.value(3).toUInt() >= 0)
    {
        InventoryObjectShp obj = InventoryObjectShp::create();
        obj->_size = _inventoryData.value(0).toUInt();
        obj->_row = _inventoryData.value(1).toUInt();
        obj->_column = _inventoryData.value(2).toUInt();
        obj->_itemType = _inventoryData.value(4);

        if (_inventoryData.value(3).toUInt() == 0)
        {
            obj->_itemsCount = _inventoryData.value(3).toUInt();
            _inventoryModel->updateInventoryData(obj, "");
        }
        else
        {
            QSound::play(":/sounds/bite.wav");
            _dataBase->removeItem(_inventoryData.value(3).toUInt() - 1, row, column);
            obj->_itemsCount = _inventoryData.value(3).toUInt() - 1;
            _inventoryModel->updateInventoryData(obj, _itemData.value(1));
        }
    }
}

// задаем данные перемещаемого(ых) айтема(ов) (яблока\яблок)
void Controller::setPreviousData(int previousRow, int previousColumn)
{
    _previousData = _dataBase->getPreviousData(previousRow, previousColumn);
}

// добавляем айтем в модель инвентаря
void Controller::addItem(int row, int column, int previousRow, int previousColumn)
{
    InventoryObjectShp obj = InventoryObjectShp::create();
    obj->_size = _inventoryData.value(0).toUInt();
    obj->_row = _inventoryData.value(1).toUInt();
    obj->_column = _inventoryData.value(2).toUInt();

    if (previousRow == -1 && previousColumn == -1)
        obj->_itemsCount = _inventoryData.value(3).toUInt() + 1;
    else
    {
        obj->_itemsCount = _inventoryData.value(3).toUInt() + _previousData.value(3).toUInt();
    }

    obj->_itemType = _inventoryData.value(4);

    if (_itemData.value(0) == _inventoryData.value(4))
    {
        _dataBase->updateInventoryData(obj->_itemsCount, row, column, previousRow, previousColumn);
        _inventoryModel->updateInventoryData(obj, _itemData.value(1));
    }
}

// очищаем модель и базу данных инвентарая
void Controller::clearInventory()
{
    _inventoryModel->clear();
    _inventoryModel->setDefaultData();
    _dataBase->clearInventoryData();
}
