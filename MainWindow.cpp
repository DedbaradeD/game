#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QStandardItemModel>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
    _ui->setupUi(this);

    _ui->inventoryTableView->viewport()->installEventFilter(this);
    _ui->itemTableView->viewport()->installEventFilter(this);

    _ui->inventoryTableView->setEnabled(false);
    _ui->itemTableView->setEnabled(false);
    _ui->menuButton->setEnabled(false);

    connect(_ui->newGameButton, &QPushButton::clicked, this, &MainWindow::newGame);
    connect(_ui->exitButton, &QPushButton::clicked, this, &MainWindow::exit);
    connect(_ui->menuButton, &QPushButton::clicked, this, &MainWindow::showMenu);
}

MainWindow::~MainWindow()
{
    delete _ui;
}

// устанавливаем модель айтема (яблока)
void MainWindow::setItemModel(QStandardItemModel *itemModel)
{
    _ui->itemTableView->setModel(itemModel);
}

// устанавливаем модель инвентаря
void MainWindow::setInventoryModel(QStandardItemModel *inventoryModel)
{
    _ui->inventoryTableView->setModel(inventoryModel);
}

// новая игра
void MainWindow::newGame()
{
    emit clearInventory();
    _ui->inventoryTableView->setEnabled(true);
    _ui->itemTableView->setEnabled(true);
    _ui->menuButton->setEnabled(true);
    _ui->newGameButton->setEnabled(false);
    _ui->exitButton->setEnabled(false);
}

// выйти из игры
void MainWindow::exit()
{
    this->close();
}

// показать меню
void MainWindow::showMenu()
{
    _ui->inventoryTableView->setEnabled(false);
    _ui->itemTableView->setEnabled(false);
    _ui->menuButton->setEnabled(false);
    _ui->newGameButton->setEnabled(true);
    _ui->exitButton->setEnabled(true);
}

// фильтр на события
bool MainWindow::eventFilter(QObject *object, QEvent *event)
{
    auto row = _ui->inventoryTableView->rowAt(
                _ui->inventoryTableView->mapFromGlobal(QCursor::pos()).y());
    auto column = _ui->inventoryTableView->columnAt(
                _ui->inventoryTableView->mapFromGlobal(QCursor::pos()).x());

    emit setInventoryData(row, column);

    if (object == _ui->inventoryTableView->viewport() && event->type() == QEvent::MouseButtonPress)
        if (((QMouseEvent*)event)->button() == Qt::RightButton)
            emit rightButtonClick(row, column);

    auto previousRow = _ui->inventoryTableView->currentIndex().row();
    auto previousColumn = _ui->inventoryTableView->currentIndex().column();

    emit setPreviousData(previousRow, previousColumn);

    if ((object == _ui->itemTableView->viewport() || object == _ui->inventoryTableView->viewport()) &&
         event->type() == QEvent::MouseButtonPress)
         if (((QMouseEvent*)event)->button() == Qt::LeftButton)
         {
            _ui->inventoryTableView->setCurrentIndex(_ui->inventoryTableView->model()->index(-1, -1));
            _ui->itemTableView->setCurrentIndex(_ui->itemTableView->model()->index(-1, -1));
         }

    if (object == _ui->inventoryTableView->viewport() && event->type() == QEvent::Drop)
        emit dropItem(row, column, previousRow, previousColumn);

    return false;
}
