#include "MainWindow.h"
#include "Controller.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    MainWindow mainWindow;
    mainWindow.show();
    Controller controller(&mainWindow);
    return application.exec();
}
