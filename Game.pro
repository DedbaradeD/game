QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets multimedia

TARGET = Game
TEMPLATE = app
RESOURCES = Game.qrc

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
    MainWindow.cpp \
    DataBase.cpp \
    Models/InventoryModel.cpp \
    Models/ItemModel.cpp \
    Objects/InventoryObject.cpp \
    Objects/ItemObject.cpp \
    Network.cpp \
    Controller.cpp

HEADERS  += MainWindow.h\
    DataBase.h \
    Models/InventoryModel.h \
    Models/ItemModel.h \
    Objects/InventoryObject.h \
    Objects/ItemObject.h \
    Network.h \
    Controller.h

FORMS    += MainWindow.ui
